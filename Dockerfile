FROM hashicorp/packer:light
LABEL maintainer="Team Online <itonlineteam@jppol.dk>"

RUN apk add xorriso
#RUN apk add --no-cache curl && curl -SL https://github.com/rgl/packer-provisioner-windows-update/releases/download/v0.11.0/packer-provisioner-windows-update_0.11.0_linux_amd64.tar.gz | tar -xzC /bin/ && chmod a+x /bin/packer-provisioner-windows-update

ENTRYPOINT ["/bin/packer"]
